﻿using System;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class Logger
    {
        private static Logger instance;
        private string FilePath;
        public string filePath
        {
                set { FilePath = value; }
        }
   
        private Logger()
        {
            this.FilePath = new string("c:\\msg.txt");
        }
        public static Logger GetInstance()
        {
            if (instance == null)
            {
                instance = new Logger();
            }
            return instance;
        }
        public void Log(string msg,bool append)
        {
            using (System.IO.StreamWriter writer =new System.IO.StreamWriter(this.FilePath, append))
            {
                writer.WriteLine(msg);
            }
        }
    }
}

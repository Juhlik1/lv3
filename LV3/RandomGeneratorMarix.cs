﻿using System;
using System.CodeDom.Compiler;
using System.Collections.Generic;
using System.Text;

namespace LV3
{
    class RandomGeneratorMatrix
    {
        private static RandomGeneratorMatrix instance;
        private Random generator;
        private RandomGeneratorMatrix()
        {
            this.generator = new Random();
        }
        public static RandomGeneratorMatrix GetInstance()
        {
            if (instance == null)
            {
                instance = new RandomGeneratorMatrix();
            }
            return instance;
        }
        public double[][] GenerateMatrix(int m,int n)
        {
            double[][] matrix = new double[m][];
            for (int i = 0; i < m; i++)
            {
                matrix[i] = new double[n];

                for (int j=0; j < n; j++)
                {
                    matrix[i][j] = generator.NextDouble() * (1 - 0) +0;
                }
            }
            return matrix;
        }
    }
}
